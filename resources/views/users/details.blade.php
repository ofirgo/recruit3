@extends('layouts.app')

@section('title', 'Details')

@section('content')


<h1>List of details</h1>
<table class = "table table-dark">
    <tr>
        <th>Id</th><th>Name</th><th>Email</th><th>Created</th><th>Updated</th><th>Department</th>
    </tr>
    <!-- the table data -->

        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>       
            <td>{{$user->created_at}}</td>
            <td>{{$user->updated_at}}</td> 
            <td>
            @if(Gate::allows('is-admin'))
             <form method="POST" action="{{ route('user.changedepartment') }}">
                    @csrf  
                    <div class="form-group row">
                    <label for="department_id" class="col-md-4 col-form-label text-md-right">Choose department</label>
                    <div class="col-md-6">
                        <select class="form-control" name="department_id">                                                                         
                          @foreach($departments as $department)
                          <option value="{{ $department->id }}"> 
                              {{ $department->name }} 
                          </option>
                          @endforeach    
                        </select>
                    </div>
                    <input name="id" type="hidden" value = {{$user->id}} >
                    <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Change department
                                </button>
                            </div>
                    </div>                    
                </form> 
                @else
                    {{$user->department->name}}
                @endif
              
            </td>
                                                              
        </tr>

</table>

@endsection

