@extends('layouts.app')

@section('title', 'Users')

@section('content')

@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif

<h1>List of users</h1>
<table class = "table table-dark">
    <tr>
        <th>Id</th><th>Name</th><th>Email</th><th>Department</th><th>Status</th><th>Created</th><th>Updated</th><th>Delete</th>
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->department->name}}</td>
            <td>
                @foreach(App\Role::rolesName($user->id) as $role)
                    {{$role->name}}
                @endforeach  
            </td>     
            <td>{{$user->created_at}}</td>
            <td>{{$user->updated_at}}</td> 
            <td>
                <a href = "{{route('user.details',$user->id)}}">Details</a>
            </td> 
            <td>
            @if(null !=App\Role::rolesName($user->id))
                @foreach(App\Role::rolesName($user->id) as $role)
                    @if($role->name=='manager')
                        <a href = "{{route('user.cancelmanager',$user->id)}}">Cancel manager</a>    
                    @else
                        <a href = "{{route('user.makemanager',$user->id)}}">Make manager</a>
                    @endif
                @endforeach 
            @else 
            <form method="POST" action="{{ route('user.makenewmanager') }}">
            <input name="id" type="hidden" value = {{$user->id}} >
                    <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Make manager
                                </button>
                            </div>
                    </div>                    
                </form> 
           
            @endif
            </td>

            <td>
                <a href = "{{route('user.delete',$user->id)}}">Delete</a>
            </td>                                                                
        </tr>
    @endforeach
</table>
@endsection

