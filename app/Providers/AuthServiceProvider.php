<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('change-status', function ($user, $candidate) {
            return $user->id === $candidate->user_id;
        });

        Gate::define('assign-user', function ($user) {
            return $user->isManager();
        }); 
// Ofir
        Gate::define('delete-user', function ($user) {
            return $user->isAdmin();
        });       
        Gate::define('is-admin', function ($user) {
            return $user->isAdmin();
        });           
        
        // Gate::define('change-deaprtment', function ($user, $candidate) {
        //     return $user->id === $candidate->user_id;
        // });
        Gate::define('change-department', function ($user) {
            return $user->chcekOwner();
        });

        //
    }
}
