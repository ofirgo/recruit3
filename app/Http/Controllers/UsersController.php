<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Department;
use App\Role;
use App\Candidate;
use App\Userrole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $users = User::all();
        $departments = Department::all();   
        $roles = Role::all();
  
        return view('users.index', compact('users', 'departments','roles'));
    }
    // Ofir
    public function details($id)
    {
        $user= User::findOrFail($id);
        $departments = Department::all();
        return view('users.details', compact('user','departments'));
    }
    // Ofir
    public function changeDepartment(Request $request)
    {
      // Gate::authorize('is-admin')
        $uid = $request->id;
        $did = $request->department_id;
      if(Gate::allows('change-department')){
      $user = User::findOrFail($uid);
        $user_id = $uid;
        $user->department_id = $did;
        $user->save(); 
        }else{
            Session::flash('notallowed','You are have candidates you are not allowed to change the department ');  
        }
        return redirect('users');

    }

    
    // Ofir
    public function makeManager($id)
    {   
         Gate::authorize('is-admin');
        $user= User::findOrFail($id);
        $userole = Userrole::findOrFail($id);
        $userole ->user_id=$user;
        $userole->role_id=2;
        $user->save(); 
        return redirect('users'); 
    
    }
    // Ofir
    public function cancelManager($id)
    {   Gate::authorize('is-admin');
        $user= User::findOrFail($id);
        $userole = Userrole::findOrFail($id);
        $userole ->user_id=$user;
        $userole->role_id=0;
        $user->save();
        return redirect('users');
    }
    public function makeNewManager(Request $request)
    {       $uid = $request->id;
            $user= User::findOrFail($uid);
            $userole->user_id=$user;
            $userole->role_id=2;
            $userole->save();
            return redirect('users'); 
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('delete-user');
        $user = User::findOrFail($id);
        $user->delete(); 
        return redirect('users'); 
    }
}
